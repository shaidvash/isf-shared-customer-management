import json
import os
import sys
import git

ENV = sys.argv[1]
PROJECT_DIR = sys.argv[2]
ORIGIN_URL = sys.argv[3]
APP_USER = sys.argv[4]
APP_PASS = sys.argv[5]
DEVOPS_DIR = "devops"
TAG_FILE_NAME = "TAG.json"
EXIT_CODE = -1

if ENV != "prod":
    exit(0)


def read_tag_from_file():
    try:
        with open(f"{PROJECT_DIR}/{TAG_FILE_NAME}", 'r') as json_file:
            version = json.load(json_file)
            return version['major'], version['minor']
    except IOError:
        print("Error: File does not appear to exist.")
        exit(EXIT_CODE)


def crate_new_tag(version_major, version_minor) -> None:
    # clone the repository
    os.chdir(DEVOPS_DIR)
    bb_repo_name = ORIGIN_URL.split('/')[-1].split('.')[0]
    git.Git().clone(f"https://{APP_USER}:{APP_PASS}@bitbucket.org/cyberarkhexagon/{bb_repo_name}.git")

    # cd to the repository
    repo = git.Repo(bb_repo_name)
    os.chdir(bb_repo_name)

    # get latest TAG
    if len(repo.tags) > 0:
        latest_tag = sorted(repo.tags, key=lambda t: t.commit.committed_date)[-1]
        latest_tag_build_build_number = latest_tag.name.split('.')[2]
        latest_tag_major = latest_tag.name.split('.')[0]
        latest_tag_minor = latest_tag.name.split('.')[1]
    else:
        latest_tag_major = version_major
        latest_tag_minor = version_minor
        latest_tag_build_build_number = 0

    # create new TAG and push to remote
    if latest_tag_major != version_major or latest_tag_minor != version_minor:
        new_tag = f"{version_major}.{version_minor}.0"
    else:
        new_build_number = int(latest_tag_build_build_number) + 1
        new_tag = f"{version_major}.{version_minor}.{new_build_number}"

    repo.create_tag(new_tag)
    repo.remotes.origin.push(new_tag)


if __name__ == '__main__':
    major, minor = read_tag_from_file()
    crate_new_tag(major, minor)

