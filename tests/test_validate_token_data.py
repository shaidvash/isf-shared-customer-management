import os
import unittest

import pytest
from pydantic import ValidationError

from kobura_customer_shared.consts import KOB_AUTHORIZER_TOKEN, CUSTOMER_NAME, CUSTOMER_USER_NAME, ORGANIZATION_ROOT
from kobura_customer_shared.data_classes.validate_token_data import ValidateTokenData


class TestValidateTokenData(unittest.TestCase):

    @pytest.fixture(autouse=True)
    def config_default_region(self):
        os.environ['AWS_DEFAULT_REGION'] = 'us-west-2'

    def test__initialize__success(self):
        jwt_token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG' \
                    '4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c'
        customer_name = 'demotenant'
        user_name = 'ttt'
        event = {
            KOB_AUTHORIZER_TOKEN: jwt_token,
            CUSTOMER_NAME: customer_name,
            CUSTOMER_USER_NAME: user_name,
        }

        data = ValidateTokenData(**event)

        self.assertEqual(data.jwt_token, jwt_token)
        self.assertEqual(data.customer_name, customer_name)
        self.assertEqual(data.user_name, user_name)

    def test__initialize__no_user_name__success(self):
        jwt_token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG' \
                    '4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c'
        customer_name = 'demotenant'
        user_name = 'ttt'
        event = {
            KOB_AUTHORIZER_TOKEN: jwt_token,
            CUSTOMER_NAME: customer_name,
        }

        data = ValidateTokenData(**event)

        self.assertEqual(data.jwt_token, jwt_token)
        self.assertEqual(data.customer_name, customer_name)
        self.assertEqual(data.user_name, ORGANIZATION_ROOT)

    def test__initialize__no_customer__failed(self):
        jwt_token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG' \
                    '4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c'
        customer_name = 'demotenant'
        user_name = 'ttt'
        event = {
            KOB_AUTHORIZER_TOKEN: jwt_token,
            CUSTOMER_USER_NAME: user_name,
        }

        try:
            data = ValidateTokenData(**event)
        except ValidationError as ex:
            self.assertEqual(len(ex.raw_errors), 1)
            if str(ex).find(CUSTOMER_NAME) != -1:
                self.assertEqual(True, True)
            else:
                self.assertEqual(False, True)

    def test__initialize__empty_string_username__failed(self):
        jwt_token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG' \
                    '4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c'
        customer_name = 'demotenant'
        user_name = ''
        event = {
            KOB_AUTHORIZER_TOKEN: jwt_token,
            CUSTOMER_NAME: customer_name,
            CUSTOMER_USER_NAME: user_name,
        }

        try:
            data = ValidateTokenData(**event)
            self.assertEqual(False, True)
        except ValidationError as ex:
            self.assertEqual(len(ex.raw_errors), 1)
            if str(ex).find(CUSTOMER_USER_NAME) != -1 \
                    and str(ex).find('value has at least') != -1:
                self.assertEqual(True, True)
            else:
                self.assertEqual(False, True)



if __name__ == '__main__':
    unittest.main()

