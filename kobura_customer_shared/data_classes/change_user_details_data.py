from kobura.validation.kob_validation import KobValidation
from kobura.validation.regex_patterns import REGEX_PHONE_NUMBER
from pydantic import Field

from kobura_customer_shared.consts import CHANGE_USER_DETAILS_FULLNAME, CHANGE_USER_DETAILS_PHONE_NUMBER, \
    CHANGE_USER_DETAILS_JOB_TITLE, CHANGE_USER_DETAILS_LAST_SING_IN


class ChangeUserDetailsData(KobValidation):
    fullname: str = Field(None, alias=CHANGE_USER_DETAILS_FULLNAME)
    phone_number: str = Field(None, alias=CHANGE_USER_DETAILS_PHONE_NUMBER, regex=REGEX_PHONE_NUMBER)
    job_title: str = Field(None, alias=CHANGE_USER_DETAILS_JOB_TITLE)
    lastSignIn: str = Field(None, alias=CHANGE_USER_DETAILS_LAST_SING_IN)

    class Config:
        max_anystr_length = 50


