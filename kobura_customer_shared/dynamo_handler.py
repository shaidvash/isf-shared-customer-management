import os
import re

import boto3
from boto3.dynamodb.conditions import Attr
from kobura.configuration.configuration_service import ConfigurationService

from kobura_customer_shared.consts import CUSTOMER_NAME, REGISTRATIONS_TABLE, CUSTOMER_HASH

kobura_region = ConfigurationService().get_region()


class DynamoHandler:

    @staticmethod
    def get_customer(item_key, table_name, consistent_read=False):
        dynamodb_resource = boto3.resource('dynamodb', region_name=kobura_region)
        table = dynamodb_resource.Table(table_name)
        item = table.get_item(Key=item_key, ConsistentRead=consistent_read)
        if not item:
            return None
        return item.get('Item')

    @staticmethod
    def put_customer(customer, table_name, fail_if_exists=False):

        dynamodb_resource = boto3.resource('dynamodb', region_name=kobura_region)
        table = dynamodb_resource.Table(table_name)
        if fail_if_exists:
            cond_attribute = f"customerName <> :custName"
            expression_values = {
                               ":custName" : customer['customerName']
                           }

            table.put_item(Item=customer,
                           ConditionExpression=cond_attribute,
                           ExpressionAttributeValues=expression_values)
        else:
            table.put_item(Item=customer)

    @staticmethod
    def delete_customer(item_key, table_name):

        dynamodb_resource = boto3.resource('dynamodb', region_name=kobura_region)
        table = dynamodb_resource.Table(table_name)
        table.delete_item(Key=item_key)

    @staticmethod
    def get_customer_stack_names():
        '''
        This actually return list of customer names, not stack names.
        we don't change signature as other projects use it.
        kob-customer-environment/migration_stacks_api_lambda.py:
        kob-customer-environment/migration_stacks_hook_lambda.py:
        kob-customer-environment/upgrade_customers/upgrade_customer_stacks.py:
        kob-customer-environment/upgrade_customers/upgrade_customer_stacks.py:
        kob-customer-environment/upgrade_customers/upgrade_customer_stacks.py:
        :return:
        '''
        customers = []
        dynamo_resource = boto3.resource('dynamodb')
        table = dynamo_resource.Table("customers")
        response = table.scan(Select='SPECIFIC_ATTRIBUTES',
                              ProjectionExpression=CUSTOMER_NAME)

        for item in response.get("Items", []):
            customers.append(item['customerName'])

        while 'LastEvaluatedKey' in response:
            response = table.scan(Select='SPECIFIC_ATTRIBUTES',
                                  ProjectionExpression=CUSTOMER_NAME,
                                  ExclusiveStartKey=response['LastEvaluatedKey'])

            for item in response.get("Items", []):
                customers.append(item['customerName'])

        return customers


    @staticmethod
    def is_customer_registration_pending(customer_name_prefix, hash_to_search):
        dynamo_resource = boto3.resource('dynamodb')
        table = dynamo_resource.Table(os.environ[REGISTRATIONS_TABLE])
        response = table.scan(Select='SPECIFIC_ATTRIBUTES',
                              ProjectionExpression=CUSTOMER_NAME,
                              FilterExpression=Attr(CUSTOMER_NAME).begins_with(customer_name_prefix) &
                                               Attr(CUSTOMER_HASH).eq(hash_to_search),
                              ConsistentRead=True)

        for item in response.get("Items", []):

            if re.match(fr'^{re.escape(customer_name_prefix)}\d{{0,4}}$', item['customerName']) is not None:
                return True

        while 'LastEvaluatedKey' in response:
            response = table.scan(Select='SPECIFIC_ATTRIBUTES',
                                  ProjectionExpression=CUSTOMER_NAME,
                                  FilterExpression=Attr(CUSTOMER_NAME).begins_with(customer_name_prefix) &
                                                   Attr(CUSTOMER_HASH).eq(hash_to_search),
                                  ExclusiveStartKey=response['LastEvaluatedKey'],
                                  ConsistentRead=True)

            for item in response.get("Items", []):
                if re.match(fr'^{re.escape(customer_name_prefix)}\d{{0,4}}$', item['customerName']) is not None:
                    return True

        return False
