import hashlib
import logging
import os
import secrets
import time
from datetime import datetime, timedelta

import jwt
from botocore.exceptions import ClientError
from deepdiff import DeepHash
from kobura.error_handling.error_codes import BAD_INPUT
from kobura.error_handling.kobura_exception import KoburaException
from kobura.license.data.license_data import License

from kobura_customer_shared.consts import CUSTOMER_NAME, CUSTOMER_CREATED_DATE, JWT_EXP_DELTA_SECONDS, \
    CUSTOMER_USER_NAME, CUSTOMER_EMAIL, \
    CUSTOMER_REGISTRATION_TOKEN, CUSTOMER_TTL, REGISTRATIONS_TABLE, JWT_ALGORITHM, TOKEN_SECRET, CUSTOMER_LICENSE, \
    CUSTOMER_REGISTRATION_DATA, CUSTOMER_TENANT_DATA, CUSTOMER_HASH
from kobura_customer_shared.dynamo_handler import *
from kobura_customer_shared.dynamo_handler import DynamoHandler

kobura_region = ConfigurationService().get_region()


class TokenHandler:

    @staticmethod
    def generate_token(customer_payload, secret, expiration_in_seconds):

        token_expiration_date = datetime.utcnow() + timedelta(seconds=expiration_in_seconds)

        payload = {
            'payload': customer_payload,
            'exp': token_expiration_date
        }
        jwt_token = jwt.encode(payload, secret, JWT_ALGORITHM)

        return jwt_token, token_expiration_date

    @staticmethod
    def validate_token(jwt_token, secret):
        try:
            payload = jwt.decode(jwt_token, secret, algorithms=[JWT_ALGORITHM])
            return payload

        except (jwt.DecodeError, jwt.ExpiredSignatureError):
            return False

    @staticmethod
    def get_customer_token_secret_from_sm(customer_name, user_name):
        try:
            secret_id = TokenHandler.extract_secret_name(customer_name, user_name)
            sm_client = boto3.client('ssm', kobura_region)
            secret = sm_client.get_parameter(Name=secret_id, WithDecryption=True)

        except Exception:
            logging.exception(f'Get token secret failed for customer: {customer_name} .')
            return None

        return secret['Parameter']['Value']

    @staticmethod
    def extract_secret_name(customer_name, user_name):
        secret_id = f'{customer_name}.{user_name}.{TOKEN_SECRET}'
        return secret_id

    @staticmethod
    def save_secret_to_secret_manager(secret, customer_name, user_name):
        secret_id = TokenHandler.extract_secret_name(customer_name, user_name)
        sm_client = boto3.client('ssm', kobura_region)

        sm_client.put_parameter(Name=secret_id,
                                Description=f'Security registration token secret of {customer_name}.',
                                Overwrite=True, Type="SecureString", Value=secret)

    @staticmethod
    def generate_secret_for_token():
        return hashlib.sha256(f'{secrets.token_hex(16)}{secrets.token_hex(16)}{datetime.utcnow()}'.encode()).hexdigest()

    @staticmethod
    def delete_token_for_customer(customer_name, user_name):

        # delete from secret manager
        secret_id = TokenHandler.extract_secret_name(customer_name, user_name)
        sm_client = boto3.client('ssm', kobura_region)
        sm_client.delete_parameter(Name=secret_id)

    @staticmethod
    def delete_token_for_customer_from_db(customer_name, user_name):
        # delete token from db
        DynamoHandler.delete_customer({CUSTOMER_NAME: customer_name, CUSTOMER_USER_NAME: user_name},
                                      os.environ[REGISTRATIONS_TABLE])


def extract_token_and_save_in_dynamo(customer_name, user_name, email, license_data: License = None,
                                     registration_data: dict = None, tenant_data: dict = None,
                                     original_tenant_name=None,
                                     fail_if_exists=True,
                                     jwt_exp_delta_seconds=JWT_EXP_DELTA_SECONDS):
    created_date = datetime.utcnow()
    customer_payload = {
        CUSTOMER_NAME: customer_name,
        CUSTOMER_CREATED_DATE: str(created_date)
    }
    token_secret = TokenHandler.generate_secret_for_token()
    jwt_token, expiration = TokenHandler.generate_token(customer_payload, token_secret, jwt_exp_delta_seconds)

    # save the new token, status and customer name in new dynamo db raw
    new_customer = {
        CUSTOMER_NAME: customer_name,
        CUSTOMER_USER_NAME: user_name,
        CUSTOMER_EMAIL: email,
        CUSTOMER_REGISTRATION_TOKEN: jwt_token,
        CUSTOMER_TTL: int(datetime_from_utc_to_local(expiration).timestamp())
    }

    customer_hash_dict = {
        CUSTOMER_USER_NAME: user_name,
        CUSTOMER_EMAIL: email,
    }

    if tenant_data:
        new_customer[CUSTOMER_TENANT_DATA] = tenant_data
        customer_hash_dict[CUSTOMER_TENANT_DATA] = tenant_data

    if license_data:
        new_customer[CUSTOMER_LICENSE] = license_data.to_dict()

    if registration_data:
        new_customer[CUSTOMER_REGISTRATION_DATA] = registration_data
        customer_hash_dict[CUSTOMER_REGISTRATION_DATA] = registration_data

    hash_data = DeepHash(customer_hash_dict)
    customer_hash = hash_data[customer_hash_dict]
    new_customer[CUSTOMER_HASH] = customer_hash

    # if original name was passed, we need to verify about pending registrations (with generated ID number)
    if original_tenant_name and DynamoHandler.is_customer_registration_pending(original_tenant_name, customer_hash):
        raise KoburaException('Customer is already pending registration.', BAD_INPUT)

    # save the secret token to secret manager
    TokenHandler.save_secret_to_secret_manager(secret=token_secret,
                                               customer_name=customer_name,
                                               user_name=user_name)

    try:
        DynamoHandler.put_customer(customer=new_customer, table_name=os.environ[REGISTRATIONS_TABLE],
                                   fail_if_exists=fail_if_exists)
    except Exception as e:
        logging.error(f"Could not save customer in dynamo, err: {e}")
        TokenHandler.delete_token_for_customer(customer_name=customer_name, user_name=user_name)
        raise

    logging.info(f'The document was saved in customer table for {customer_name}')

    return jwt_token, new_customer


def datetime_from_utc_to_local(utc_datetime):
    now_timestamp = time.time()
    offset = datetime.fromtimestamp(now_timestamp) - datetime.utcfromtimestamp(now_timestamp)
    return utc_datetime + offset
