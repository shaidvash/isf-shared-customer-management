import os

import boto3
import pytest
from moto import mock_dynamodb, mock_dynamodb2

from kobura_customer_shared.dynamo_handler import kobura_region, DynamoHandler
from kobura_customer_shared.tests_util.tests_helper import create_table, CUSTOMER_TABLE, KOBURA_REGION, \
    create_customer_row, REGISTRATIONS_TABLE


@pytest.fixture(autouse=True)
def config_default_region():
    os.environ['AWS_DEFAULT_REGION'] = 'us-west-2'

def setup_function():
    boto3.setup_default_session(region_name=kobura_region)
    os.environ[CUSTOMER_TABLE] = 'customers'
    os.environ[REGISTRATIONS_TABLE] = 'registrations'
    os.environ[KOBURA_REGION] = 'us-west-2'

def teardown_function():
    del os.environ[CUSTOMER_TABLE]
    del os.environ[KOBURA_REGION]


@mock_dynamodb2
def test_get_customers():
    create_table('customers')

    for i in range(400):
        item = {
            "bucket": f'bucket_name{i}',
            "customerName": f'cust{i}'
        }
        create_customer_row(item, 'customers')
    names = DynamoHandler.get_customer_stack_names()
    for i in range(400):
        assert names[i] == f'cust{i}'
    pass

@mock_dynamodb2
def test_get_registrations():
    create_table('registrations')

    for x in ['cust1', 'cust', 'maw', 'cmaw', 'cust5', 'cust84']:
        item = {
            "bucket": f'bucket_name{x}',
            "customerName": x,
            "hash": "a"
        }
        create_customer_row(item, 'registrations')
    assert DynamoHandler.is_customer_registration_pending('cust', "a") == True

@mock_dynamodb2
def test_get_registrations_not_relevant_customers():
    create_table('registrations')

    for x in ['cust-1', 'cust10000', 'maw']:
        item = {
            "bucket": f'bucket_name{x}',
            "customerName": x,
            "hash": "a"
        }
        create_customer_row(item, 'registrations')
    assert DynamoHandler.is_customer_registration_pending('cust', "a") == False

@mock_dynamodb2
@pytest.mark.parametrize("name", ['cust5', 'cust0', 'cust9999', 'cust', 'cust959', 'cust29'])
def test_single_registration_exists(name):
    create_table('registrations')

    item = {
        "bucket": f'bucket_name{name}',
        "customerName": name,
        "hash": "a"
    }
    create_customer_row(item, 'registrations')

    for x in ['cust-1', 'cust99994', 'bla345', 'cust1maw']:
        item = {
            "bucket": f'bucket_name{x}',
            "customerName": x,
            "hash": "a"
        }
        create_customer_row(item, 'registrations')

    assert DynamoHandler.is_customer_registration_pending('cust', "a") == True

@mock_dynamodb2
@pytest.mark.parametrize("name", ['cust5', 'cust0', 'cust9999', 'cust', 'cust959', 'cust29'])
def test_single_registration_exists_but_different_hash(name):
    create_table('registrations')

    item = {
        "bucket": f'bucket_name{name}',
        "customerName": name,
        "hash": "a"
    }
    create_customer_row(item, 'registrations')

    for x in ['cust-1', 'cust99994', 'bla345', 'cust1maw']:
        item = {
            "bucket": f'bucket_name{x}',
            "customerName": x,
            "hash": "a"
        }
        create_customer_row(item, 'registrations')

    assert DynamoHandler.is_customer_registration_pending('cust', "d") == False


@mock_dynamodb2
def test_put_same_registration():
    create_table('registrations')
    item = {
        "bucket": f'bucket_name',
        "customerName": 'a'
    }
    create_customer_row(item, 'registrations')
    with pytest.raises(boto3.resource('dynamodb').meta.client.exceptions.ConditionalCheckFailedException):
        DynamoHandler.put_customer(item, 'registrations', True)

@mock_dynamodb2
def test_put_customer_ok():
    create_table('registrations')
    item = {
        "bucket": f'bucket_name',
        "customerName": 'b',
        "hash": "maw"
    }
    DynamoHandler.put_customer(item, 'registrations', True)


