INTERNAL_EMAIL = 'isf@cyberark.com'

# common parts
GENERIC_MAIL_HTML = """
<html>
<head>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>{fullSystemName}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
<table align="center" width="100%">
    <tr>
        <td>
            <table align="center" width="600px" height="80px" style="border: 1px solid #cccccc;">

                <tr>
                    <td>

                        <!-- Outlook template -->
                        <!--[if mso]>
                            {outlookPng}
                        <![endif]-->

                        <!-- Gmail template -->
                        <!--[if !mso]>-->
                            {imgTag}
                        <!--<![endif]-->
                    </td>
                </tr>

                {bodyText}

                {buttonArea}

                <tr>
                   <td align="center">
                    <hr>
                   </td>
                  </tr>
                    <tr>
                   <td align="center" style="width: 368px; height: 60px; font-family: OpenSans; font-size: 12px; line-height: 1.58; text-align: center; color: #787878;">
                    Copyright © {year} CyberArk Software Ltd. All rights reserved.<br> <a href="https://www.cyberark.com/terms-service-saas/">Terms of Service</a> | <a href="https://www.cyberark.com/privacy-policy/"> Privacy Policy</a>
                   </td>
                  </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
"""

NO_BUTTON = """
    <tr>
    </tr>
"""
GENERIC_BUTTON = """
    <tr>
        <td bgcolor="#ffffff" style="padding: 0px 30px 30px 30px;">
            <table align="center" width="280px" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="center" style="border-radius:17px;width:280px;" bgcolor="#3165bd">
                        <a href="{linkUrl}" target="_blank" rel="noopener" style="width:280px;font-size: 14px; font-family: Helvetica, Arial, sans-serif; color: #ffffff; text-decoration: none; text-decoration: none;border-radius:17px; padding: 4px 0px; border: 1px solid #3165bd; display: inline-block;">{buttonText}</a></td>
                </tr>
            </table>
        </td>
    </tr>
"""

REGULAR_IMG_TAG = '<img alt="Logo" title="Logo" style="display:block" width="600" height="80" src="https://{domain}/assets/mail-templates/{imgName}">'
REGULAR_MAIL_IMG_NAME = "cem-welcome-mail-header-tm-big.png"

# welcome mail
WELCOM_MAIL_TEXT = """Hello,\r\n
Thank you for your interest in {fullSystemName}.
Click the link below to register as a {fullSystemName} admin for your organization.
\r\n
Your invitation is valid for 7 days.
{url}"""


WELCOM_MAIL_TEXT_HTML = """
      <tr>
        <td bgcolor="#ffffff" style="padding: 30px;">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td style="color:#40547b; font-size:16px; line-height:20px; font-family:'Open Sans', sans-serif; font-weight: 600; direction: ltr;">
                    <p>Hello,</p>
                    </td>
                </tr>
                <tr>
                    <td style="line-height:30px; color:#333333; font-size:14px; font-family: 'Open Sans', sans-serif; direction: ltr;">
                    Thank you for your interest in {fullSystemName}.
                    <br>
                    Click the button below to register as a {fullSystemName} admin for your organization.
                    <br>
                    Your invitation is valid for <span style="font-weight: bold;">7 days</span>.
                   </td>
                  </tr>
            </table>
        </td>
      </tr>
"""

WELCOME_BUTTON_TEXT = "Register for {fullSystemName}"

# forgot password mail
FORGOT_PASSWORD_MAIL_TEXT = """Hello {username},\r\n
We received a request to recover your {fullSystemName} password.
\r\n
Click the link below to reset your password and sign into your account.
This link is valid for 24 HOURS.
{url}"""


FORGOT_PASSWORD_MAIL_TEXT_HTML = """
      <tr>
        <td bgcolor="#ffffff" style="padding: 40px 30px 50px 30px;">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td style="color:#40547b; font-size:16px; line-height:20px; font-family:'Open Sans', sans-serif; font-weight: 600; direction: ltr;">
                    <p>Hello {userName},</p>
                    </td>
                </tr>
                <tr>
                    <td style="line-height:30px; color:#333333; font-size:14px; font-family: 'Open Sans', sans-serif; direction: ltr;">
                     We received a request to recover your {fullSystemName} password.
                    <br>
                    Click the button below to reset your password and sign into your account.
                    <br>
                    This button is valid for <span style="font-weight: bold;">24 HOURS</span>.
                   </td>
                  </tr>
            </table>
        </td>
      </tr>
"""

# forgot password mail
PRE_EXPIRY_EMAIL = """Hello {userFullName},\r\n
According to our records, your {licenseType} {customerName} {fullSystemName} account will end in {daysLeft} days, on {expiryDate}.
\r\n
In {daysLeft} days {fullSystemName} stops monitoring your connected accounts.
You will no longer be able to track the exposure level of your cloud platform with up to date data, or to take actions to reduce the attack surface.
\r\n
Contact your CyberArk representative to discuss licensing options."""

PRE_EXPIRY_EMAIL_HTML = """
      <tr>
        <td bgcolor="#ffffff" style="padding: 40px 30px 50px 30px;">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td style="color:#40547b; font-size:16px; line-height:20px; font-family:'Open Sans', sans-serif; font-weight: 600; direction: ltr;">
                    <p>Hello {user_name},</p>
                    </td>
                </tr>
                    <tr>
                        <td style="line-height:30px; color:#333333; font-size:14px; font-family: 'Open Sans', sans-serif; direction: ltr;">
                    According to our records, your {license_type} {customer_name} {fullSystemName} account expires in {days_left} days, on {expiry_date}.
                    <br>
                    <br>
                    In {days_left} days {fullSystemName} stops monitoring your connected accounts.
                    <br>
                    You will no longer be able to track the exposure level of your cloud platform with up to date data, or to take actions to reduce the attack surface.
                    <br>
                    <br>
                    Contact your CyberArk representative to discuss licensing options.
                   </td>
                  </tr>
            </table>
        </td>
      </tr>
"""

FORGOT_PASSWORD_BUTTON_TEXT = "Reset your password"

# follow up mails
FOLLOW_IMG_TAG = '<img alt="Logo" title="Logo" style="display:block" width="600" height="249" src="https://{domain}/assets/mail-templates/{imgName}">'

BODY_TEXT_FOLLOW_UP = """Welcome to {fullSystemName}!\r\n
Start evaluating your cloud assets and minimizing your attack surface.
\r\n
As a reminder, your sign-in details are:\r\n
Organization {customerName}
Username {userName}
Temporary password {password}\r\n
\r\n
Click the link below to access the console.
{url}"""

BODY_FOLLOW_UP_TEXT_HTML = """
<tr>
    <td bgcolor="#ffffff" style="padding: 40px 30px 30px 30px;">
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td style="color:#4c5e88; font-size:16px; line-height:20px; font-family:'Open Sans', sans-serif; font-weight: bold; direction: ltr;">
                    <p>Welcome to {fullSystemName}!</p>
                </td>
            </tr>
            <tr>
                <td style="line-height:30px; color:#666666; font-size:14px; font-family: 'Open Sans', sans-serif; direction: ltr;">
                    Start evaluating your cloud assets and minimizing your attack surface.
                    <div style="padding-top:24px;">
                    As a reminder, your sign-in details are:
                    </div>
                    <div style="background-color:#eef1f6; color:#666666; padding:20px 25px; width:320px;">
                    <div style="line-height: 20px;">
                        <div>Organization</div>
                        <div style="font-weight:bold ;color:#333333">{organization}</div>
                    </div>
                    <div style="line-height: 20px; padding-top: 15px; ">
                        <div>Username</div>
                        <div style="font-weight:bold;color:#333333">{userName}</div>
                    </div>
                    <div style="line-height: 20px; padding-top: 15px; ">
                        <div>Temporary password</div>
                        <div style="font-weight:bold;color:#333333">{password}</div>
                    </div>
                    </div>
                </td>
            </tr>
        </table>
    </td>
</tr>
"""

FOLLOW_UP_BUTTON_TEXT = "Go to {fullSystemName}"

FOLLOW_IMG_NAME = "cem-follow-up-mail-header-tm.png"

INVITATION_MAIL_TEXT = """Hello {userFullName},\r\n
{senderName} has registered you as a {fullSystemName} user.
\r\n
Your sign in details are:\r\n
Organization {customerName}
Username {userName}
Temporary password {password}\r\n
\r\n
Click the link below to sign in to {fullSystemName} for the first time.
Your temporary password is valid for 7 days.
{url}"""

INVITATION_MAIL_TEXT_HTML = """
<tr>
    <td bgcolor="#ffffff" style="padding: 40px 30px 30px 30px;">
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td style="color:#4c5e88; font-size:16px; line-height:20px; font-family:'Open Sans', sans-serif; font-weight: bold; direction: ltr;">
                    <p>Hello {userFullName},</p>
                </td>
            </tr>
            <tr>
                <td style="line-height:30px; color:#666666; font-size:14px; font-family: 'Open Sans', sans-serif; direction: ltr;">
                    {senderName} has registered you as a {fullSystemName} user.
                    <div style="padding-top:24px;">
                    Your sign in details are:
                    </div>
                    <div style="background-color:#eef1f6; color:#666666; padding:20px 25px; width:320px;">
                    <div style="line-height: 20px;">
                        <div>Organization</div>
                        <div style="font-weight:bold ;color:#333333">{organization}</div>
                    </div>
                    <div style="line-height: 20px; padding-top: 15px; ">
                        <div>Username</div>
                        <div style="font-weight:bold;color:#333333">{userName}</div>
                    </div>
                    <div style="line-height: 20px; padding-top: 15px; ">
                        <div>Temporary password</div>
                        <div style="font-weight:bold;color:#333333">{password}</div>
                    </div>
                    </div>
                    <div style="padding-top:24px;">
                    Click the button below to sign in to {fullSystemName} for the first time.
                    </div>
                    Your temporary password is valid for <span style="font-weight: bold;">7 days</span>.
                </td>
            </tr>
        </table>
    </td>
</tr>
"""
