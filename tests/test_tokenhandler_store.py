import os

import boto3
import pytest
from moto import mock_ssm
from moto.ssm.exceptions import ParameterNotFound

from kobura_customer_shared.tests_util.tests_helper import CUSTOMER_TABLE, KOBURA_REGION, \
                                                            REGISTRATIONS_TABLE
from kobura_customer_shared.dynamo_handler import kobura_region, DynamoHandler
from kobura_customer_shared.token_handler import TokenHandler


@pytest.fixture(autouse=True)
def config_default_region():
    os.environ['AWS_DEFAULT_REGION'] = 'us-west-2'


def setup_function():
    boto3.setup_default_session(region_name=kobura_region)
    os.environ[CUSTOMER_TABLE] = 'customers'
    os.environ[REGISTRATIONS_TABLE] = 'registrations'
    os.environ[KOBURA_REGION] = 'us-west-2'


def teardown_function():
    del os.environ[CUSTOMER_TABLE]
    del os.environ[KOBURA_REGION]


@mock_ssm
def test_store_parameter():
    customer_name = "TestCustomer"
    user_name = "TestUser"

    secret_name = TokenHandler.extract_secret_name(customer_name, user_name)
    TokenHandler.save_secret_to_secret_manager(secret="secret_data_inside!",
                                               customer_name="TestCustomer",
                                               user_name="TestUser")

    client = boto3.client("ssm")
    stored_parameter = client.get_parameter(Name=secret_name, WithDecryption=True)
    assert stored_parameter['Parameter']['Name'] == secret_name


@mock_ssm
def test_get_parameter():
    customer_name = "TestCustomer"
    user_name = "TestUser"

    secret_name = TokenHandler.extract_secret_name(customer_name, user_name)
    TokenHandler.save_secret_to_secret_manager(secret="secret_data_inside!",
                                               customer_name="TestCustomer",
                                               user_name="TestUser")

    secret_data = TokenHandler.get_customer_token_secret_from_sm(customer_name, user_name)
    assert secret_data == "secret_data_inside!"


@mock_ssm
def test_update_parameter():
    customer_name = "TestCustomer"
    user_name = "TestUser"

    secret_name = TokenHandler.extract_secret_name(customer_name, user_name)
    TokenHandler.save_secret_to_secret_manager(secret="secret_data_inside!",
                                               customer_name="TestCustomer",
                                               user_name="TestUser")

    TokenHandler.save_secret_to_secret_manager(secret="secret_updated_data_inside!",
                                               customer_name="TestCustomer",
                                               user_name="TestUser")

    client = boto3.client("ssm")
    stored_parameter = client.get_parameter(Name=secret_name, WithDecryption=True)
    assert stored_parameter['Parameter']['Name'] == secret_name and \
           stored_parameter['Parameter']['Value'] == "secret_updated_data_inside!"


@mock_ssm
def test_delete_parameter():
    customer_name = "TestCustomer"
    user_name = "TestUser"

    secret_name = TokenHandler.extract_secret_name(customer_name, user_name)
    TokenHandler.save_secret_to_secret_manager(secret="secret_data_inside!",
                                               customer_name="TestCustomer",
                                               user_name="TestUser")

    TokenHandler.delete_token_for_customer(customer_name, user_name)
    client = boto3.client("ssm")

    try:
        stored_parameter = client.get_parameter(Name=secret_name, WithDecryption=True)
    except Exception as e:
        assert True

    assert False