# kobura region env variable
KOBURA_REGION = 'kobura_region'

# status codes

OK = 200
ERROR = 500
UNAUTHORIZED = 401

# Change User Details API
CHANGE_USER_DETAILS_USER_NAME = 'userName'
CHANGE_USER_DETAILS_FULLNAME = 'fullName'
CHANGE_USER_DETAILS_PHONE_NUMBER = 'phoneNumber'
CHANGE_USER_DETAILS_JOB_TITLE = 'jobTitle'
CHANGE_USER_DETAILS_LAST_SING_IN = 'lastSignIn'

# dynamodb
ITEM_FIELD = 'Item'

# customer info
CUSTOMER_LICENSE = 'license'
CUSTOMER_HASH = 'hash'
CUSTOMER_OVERRIDE_EXISTING_LICENSE = 'overrideExistingLicenseIfExist'
CUSTOMER_NAME = 'customerName'
CUSTOMER_EMAIL = 'customerEmail'
CUSTOMER_GLOBAL_ID = 'customerGlobalID'
CUSTOMER_REGISTRATION_TOKEN = 'customerRegistrationToken'
CUSTOMER_REGISTRATION_DATA = 'customerRegistrationData'
CUSTOMER_TENANT_DATA = 'customerTenantData'
CUSTOMER_TTL = 'ttl'
CUSTOMER_USER_NAME = 'userName'
ORGANIZATION_ROOT = "organizationRoot"
CUSTOMER_CREATED_DATE = 'createdDate'

# Add User API
ADD_USER_USER_NAME = 'userName'
ADD_USER_ORGANIZATION = 'organization'
ADD_USER_EMAIL = 'email'
ADD_USER_PHONE = 'phoneNumber'
ADD_USER_LAST_SIGN_IN = 'lastSignIn'
ADD_USER_JOB_TITLE = 'jobTitle'
ADD_USER_FULL_NAME = 'fullName'
ADD_USER_PASSWORD = 'password'
ADD_USER_USER_POOL_ID = 'userPoolId'
ADD_USER_IDENTITY_POOL_ID = 'identityPoolId'
ADD_USER_USER_GROUP = 'userGroup'
ADD_USER_IS_SEND_WELCOME_MAIL = 'isSendWelcomeMail'

# environments
CUSTOMER_TABLE = 'CUSTOMER_TABLE'
LICENSES_TABLE = 'LICENSES_TABLE'
POOLS_TABLE = 'POOLS_TABLE'
DOMAIN_NAME = 'DOMAIN_NAME'
REGISTRATIONS_TABLE = 'REGISTRATIONS_TABLE'
CUSTOMER_NEW_ENVIRONMENT_LAMBDA_ARN = 'CUSTOMER_NEW_ENVIRONMENT_LAMBDA_ARN'
GENERATE_FIRST_USER_ENVIRONMENT_LAMBDA_ARN = 'GENERATE_FIRST_USER_ENVIRONMENT_LAMBDA_ARN'
#license
PRE_EXPIRY_MAIL_DAYS_NAME = 'expiryDaysBeforeNotification'
TRIAL_TEXT = "free trial"

# status
PENDING_REGISTRATION = 'PENDING_REGISTRATION'

# JWT
JWT_EXP_DELTA_SECONDS = 1 * 24 * 60 * 60  # 86400 seconds in a day
JWT_ALGORITHM = 'HS256'
TOKEN_ATTRIBUTE_NAME = 'customerRegistrationToken'

# email
KOBURA_WELCOME_SUBJECT = "Welcome to Kobura"
WELCOME_SUBJECT = "Welcome to {fullSystemName}"
KOBURA_FORGOT_PASSWORD_SUBJECT = "Kobura forgot password"
FORGOT_PASSWORD_SUBJECT = "{fullSystemName} forgot password"
KOBURA_EXPIRY_DAYS_TO_GO = "{} days left until your {} Kobura account expires"
EXPIRY_DAYS_TO_GO = "{} days left until your {} {} account expires"
KOBURA_FOLLOW_SUBJECT = "Kobura sign up"
FOLLOW_SUBJECT = "{fullSystemName} sign up"
SENDER = "no-reply@{}"
SIGNUP_URL = "https://{}/signup?id={}&organization={}"
SIGNIN_URL = "https://{}/signin"
RESET_PASSWORD_URL = "https://{}/signin/reset-password?id={}&organization={}&username={}"
KOBURA_IMG_URL = "https://{domain}/assets/mail-templates/{imgName}"
CHARSET = "UTF-8"

# mail options values
DESTINATIONS_OPTION = 'destinations'
SOURCE_OPTION = 'source'
SUBJECT_OPTION = 'subject'

# customer stack outputs values
OUTPUT_USER_POOL = 'user_pool'
OUTPUT_IDENTITY_POOL = 'identity_pool'
OUTPUT_APP_CLIENT = 'app_client'
OUTPUT_API_APP_CLIENT = 'api_app_client'
OUTPUT_IDP_ROLE_AUTH = 'idp_role_auth'
OUTPUT_IDP_ROLE_UNAUTH = 'idp_role_unauth'
OUTPUT_S3_BUCKET_NAME = 'bucket_name'

# BODY_TEXT_RESET_PASSWORD = """Hello {},\r\n
#              We received a request to recover your Kobura password.\r\n
#              Your confirmation code is {}\r\n
#              """
SUBJECT_RESET_PASSWORD = "Kobura password reset"

# login details
KOB_LOGIN_ORGANIZATION = 'organization'
KOB_LOGIN_USERNAME = 'userName'
KOB_LOGIN_PASSWORD = 'password'
KOB_LOGIN_CONFIRMATION_CODE = 'confirmationCode'

# authorizer
KOB_AUTHORIZER_NAME = 'authorizer'

KOB_AUTHORIZER_TOKEN = 'token'
KOB_AUTHORIZER_CUSTOMER_NAME = 'customerName'
KOB_AUTHORIZER_COGNITO_GROUP = 'cognitoGroup'
KOB_AUTHORIZER_COGNITO_ROLE = 'cognitoRole'
KOB_AUTHORIZER_COGNITO_USERNAME = 'cognitoUsername'
KOB_AUTHORIZER_USER_POOL_ID = 'userPoolId'
KOB_AUTHORIZER_IDENTITY_POOL_ID = 'identityPoolId'
KOB_AUTHORIZER_APP_CLIENT_ID = 'appClientId'

# pools table
KOB_POOLS_CUSTOMER_NAME = 'customerName'
KOB_POOLS_USER_POOL_ID = 'userPoolId'
KOB_POOLS_OLD_USER_POOL_ID = 'old_userPoolId'
KOB_POOLS_IDENTITY_POOL_ID = 'identityPoolId'
KOB_POOLS_APP_CLIENT_ID = 'appClientId'
KOB_POOLS_OLD_APP_CLIENT_ID = 'old_appClientId'


EVENT_USER_NAME = 'userName'
EVENT_ORGANIZATION = 'organization'
EVENT_EMAIL = 'email'
EVENT_PHONE = 'phoneNumber'
EVENT_JOB_TITLE = 'jobTitle'
EVENT_FULL_NAME = 'fullName'
EVENT_PASSWORD = 'password'
EVENT_USER_POOL_ID = 'userPoolId'
EVENT_IDENTITY_POOL_ID = 'identityPoolId'
EVENT_USER_GROUP = 'userGroup'
EVENT_IS_SEND_WELCOME_MAIL = 'isSendWelcomeMail'

# customer cloud formation stack
CUSTOMER_CF_TEMPLATE_FILENAME = 'cf.template'


# Challenge Name And Types
AUTH_RESPONSE_CHALLENGE_NAME = 'ChallengeName'

CHALLENGE_NAME_SMS_MFA = 'SMS_MFA'
CHALLENGE_NAME_SOFTWARE_TOKEN_MFA = 'SOFTWARE_TOKEN_MFA'
CHALLENGE_NAME_NEW_PASSWORD_REQUIRED = 'NEW_PASSWORD_REQUIRED'


COOKIE_HEADER_MAP = [
    'Set-cookie',
    'SEt-cookie',
    'SET-cookie',
    'SeT-cookie',
    'set-cookie',
    'sEt-cookie',
    'seT-cookie',
    'sET-cookie',
    'set-Cookie',
    'set-COokie',
    'set-COOkie',
    'set-COOKie',
    'set-COOKIe',
    'set-COOKIE',
    'set-cOOKIE',
    'set-coOKIE',
    'set-cooKIE',
    'set-cookIE',
    'set-cookiE',
    'set-cookie',
]

LOGIN_ERROR = 402
LOGIN_RESET_PASSWORD_REQUIRED_ERROR = 407
USER_IS_DISABLED_OR_UNAUTHORIZED = 403
INVALID_VERIFICATION_CODE = 406
INVALID_PARAMETER = 408
USER_NOT_FOUND = 409
NEW_PASSWORD_CONFIRMATION_FAILED = 410
FORGOT_PASSWORD_CONFIRMATION_FAILED = 411
MFA_FAILED = 412
ASSOCIATE_SOFTWARE_TOKEN_FAILED = 413
USER_CHANGE_PASSWORD_USERNAME_OR_PASSWORD_INCORRECT = 414
REFRESH_TOKEN_ERROR = 418

LOGIN_ERROR_RESPONSE = {
    'statusCode': 400,
    'headers': {}
}

NEW_PASSWORD_CONFIRMATION_ERROR_RESPONSE = {
    'statusCode': 400,
    'headers': {}
}

MFA_CONFIRMATION_ERROR_RESPONSE = {
    'statusCode': 400,
    'headers': {}
}

ASSOCIATE_SOFTWARE_TOKEN_ERROR_RESPONSE = {
    'statusCode': 400,
    'headers': {}
}

FORGOT_PASSWORD_CONFIRMATION_ERROR_RESPONSE = {
    'statusCode': 400,
    'headers': {}
}

REFRESH_TOKEN_ERROR_RESPONSE = {
    'statusCode': 401,
    'headers': {}
}

TOKEN_SECRET = 'token-secret'

AWS_REGION = "us-west-2"
SUBJECT = "Kobura - New demo request"
RECIPIENTS = ["kobura@cyberark.com"]

ONE_DAY_IN_SECONDS = 86400
