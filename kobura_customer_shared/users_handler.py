import time
import boto3

from datetime import datetime

from kobura.configuration.configuration_service import ConfigurationService
from kobura.enums.user_groups_enum import UserGroups
from kobura_customer_shared.data_classes.change_user_details_data import ChangeUserDetailsData


def get_cognito_user_attributes(attributes):
    if not attributes:
        return {}
    formatted_attributes = {}
    for attribute in attributes:
        if attribute['Name'] == 'email':
            formatted_attributes.update({'email': attribute['Value']})
        if attribute['Name'] == 'name':
            formatted_attributes.update({'fullName': attribute['Value']})
        if attribute['Name'] == 'custom:organization':
            formatted_attributes.update({'organization': attribute['Value']})
        if attribute['Name'] == 'phone_number':
            formatted_attributes.update({'phoneNumber': attribute['Value']})
        if attribute['Name'] == 'custom:role':
            formatted_attributes.update({'jobTitle': attribute['Value']})
        if attribute['Name'] == 'custom:type':
            formatted_attributes.update({'userGroup': attribute['Value']})
        if attribute['Name'] == 'custom:lastSignIn':
            formatted_attributes.update({'lastSignIn': attribute['Value']})
        if attribute['Name'] == 'custom:accessType':
            formatted_attributes.update({'accessType': attribute['Value']})
    return formatted_attributes


def extract_user_attributes(data: ChangeUserDetailsData):

    user_attributes = []

    if data.fullname:
        user_attributes.append({'Name': 'name', 'Value': data.fullname})
    if data.phone_number:
        user_attributes.append({'Name': 'phone_number', 'Value': data.phone_number})
    if data.job_title:
        user_attributes.append({'Name': 'custom:role', 'Value': data.job_title})
    if data.lastSignIn:
        user_attributes.append({'Name': 'custom:lastSignIn', 'Value': data.lastSignIn})

    return user_attributes


def is_valid_group(group):
    return group in [UserGroups.admins.value, UserGroups.users.value]


def datetime_from_utc_to_local(utc_datetime):
    now_timestamp = time.time()
    offset = datetime.fromtimestamp(now_timestamp) - datetime.utcfromtimestamp(now_timestamp)
    return utc_datetime + offset


def confirmed_users_from_group_iterator(group_name, pool_id):
    cognito_client = boto3.client('cognito-idp')
    first_time = True
    while first_time or 'NextToken' in response:
        if first_time:
            first_time = False
            response = cognito_client.list_users_in_group(UserPoolId=pool_id, GroupName=group_name)
        else:
            response = cognito_client.list_users_in_group(UserPoolId=pool_id, GroupName=group_name,
                                                          NextToken=response['NextToken'])
        users_list = response.get('Users', [])
        for user in users_list:
            status = user.get('UserStatus', None)
            if user.get('Enabled', False) and (status == 'CONFIRMED' or status == 'RESET_REQUIRED' or status == 'FORCE_CHANGE_PASSWORD'):
                cognito_user = {
                    'userName': user['Username']
                }
                attributes = user.get('Attributes')
                cognito_user.update(get_cognito_user_attributes(attributes))
                yield cognito_user


def get_user_details_as_admin_internal(username, pool_id):
    cognito_client = boto3.client('cognito-idp')

    response = cognito_client.admin_get_user(UserPoolId=pool_id, Username=username)

    cognito_user = {
        'userName': response['Username'],
        'enabled': response['Enabled'],
        'status': response['UserStatus'],
        'createdDate': str(response['UserCreateDate']),
        'preferredMFA': response.get('PreferredMfaSetting', 'MFA_NONE')
    }

    attributes = response.get('UserAttributes')

    cognito_user.update(get_cognito_user_attributes(attributes))

    return cognito_user


def change_user_details_as_user_internal(access_token, user_details_data: ChangeUserDetailsData):
    kobura_region = ConfigurationService().get_region()

    user_attributes = extract_user_attributes(user_details_data)

    cognito_client = boto3.client('cognito-idp', kobura_region)

    cognito_client.update_user_attributes(
        AccessToken=access_token,
        UserAttributes=user_attributes)
