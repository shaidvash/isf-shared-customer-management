import json
import os
import unittest
import boto3
import pytest
from freezegun import freeze_time
from kobura.audit.audit_message_data import AuditMessageData
from kobura.enums.audit_event_status import AuditEventStatus
from kobura.enums.audit_event_type import AuditEventType
from moto import mock_sns, mock_sqs

from kobura_customer_shared.audit_helper import audit_user_action

# boto3.setup_default_session(region_name=ConfigurationService().get_region())


class TestAuditHelper(unittest.TestCase):
    customer_name = 'mb35'
    topic_prefix = 'kobura-events-'
    topic_name = f'{topic_prefix}{customer_name}'
    user_name = 'blabla'

    @pytest.fixture(autouse=True)
    def config_default_region(self):
        os.environ['AWS_DEFAULT_REGION'] = 'us-west-2'

    def setUp(self):
        boto3.setup_default_session(region_name='us-west-2')
        topic_arn_prefix = 'arn:aws:sns:us-west-2:123456789012:kobura-events-'
        os.environ['CUSTOMER_EVENTS_TOPIC_ARN_PREFIX'] = topic_arn_prefix

    @mock_sqs
    @mock_sns
    def test_audit_login(self):
        conn = boto3.client("sns")
        conn.create_topic(Name=self.topic_name)
        response = conn.list_topics()
        topic_arn = response["Topics"][0]["TopicArn"]

        sqs_conn = boto3.resource("sqs")
        endpoint_arn = sqs_conn.create_queue(QueueName="test-queue-add-account").attributes['QueueArn']

        target_username = 'user1'
        target_usergroup = 'users'

        conn.subscribe(
            TopicArn=topic_arn,
            Protocol="sqs",
            Endpoint="arn:aws:sqs:us-west-2:123456789012:test-queue-add-account",
        )
        with freeze_time("2015-01-01 12:00:00"):
            send_message_result = audit_user_action(caller_user_name=self.user_name,
                                                    customer=self.customer_name,
                                                    event_type=AuditEventType.signedIn,
                                                    event_status=AuditEventStatus.success,
                                                    caller_user_group='admins',
                                                    target_user_name=target_username,
                                                    target_user_group=target_usergroup)

        queue = sqs_conn.get_queue_by_name(QueueName="test-queue-add-account")
        messages = queue.receive_messages(MaxNumberOfMessages=1)

        received_message = messages[0].body
        sns_message_body_dict = json.loads(received_message)
        parsed_message = AuditMessageData.from_json_string(sns_message_body_dict.get('Message'))

        assert send_message_result
        assert parsed_message.username == self.user_name
        assert parsed_message.user_group.value == "admins"
        assert parsed_message.customer_name == self.customer_name
        assert parsed_message.datetime == '2015-01-01T12:00:00+00:00'
        assert parsed_message.event_type == AuditEventType.signedIn

        details = parsed_message.event_details
        assert len(details) == 2
        assert details.get('userName', '') == target_username
        assert details.get('userRole', '') == target_usergroup

        assert parsed_message.event_status == AuditEventStatus.success
        assert parsed_message.event_type == AuditEventType.signedIn

    @mock_sqs
    @mock_sns
    def test_audit_login_fail_without_username(self):
        conn = boto3.client("sns")
        conn.create_topic(Name=self.topic_name)
        response = conn.list_topics()
        topic_arn = response["Topics"][0]["TopicArn"]

        sqs_conn = boto3.resource("sqs")
        sqs_conn.create_queue(QueueName="test-queue-add-account")
        user_name = None
        conn.subscribe(
            TopicArn=topic_arn,
            Protocol="sqs",
            Endpoint="arn:aws:sqs:us-west-2:622382967619:test-queue-add-account",
        )
        with freeze_time("2015-01-01 12:00:00"):
            send_message_result = audit_user_action(caller_user_name=user_name,
                                                    customer=self.customer_name,
                                                    event_type=AuditEventType.signedIn,
                                                    event_status=AuditEventStatus.success,
                                                    caller_user_group='admins',
                                                    target_user_name='user1',
                                                    target_user_group='users')

        assert not send_message_result
