import logging

from kobura.audit.audit_trail_service import AuditTrailService
from kobura.consts import AUDIT_EVENT_DETAILS_USER_ROLE, AUDIT_EVENT_DETAILS_USER_NAME
from kobura.enums.audit_event_status import AuditEventStatus
from kobura.enums.audit_event_type import AuditEventType


def audit_user_action(caller_user_name: str,
                      customer: str,
                      event_type: AuditEventType,
                      event_status: AuditEventStatus,
                      target_user_name: str,
                      caller_user_group: str,
                      target_user_group: str = None) -> bool:

    audit_additional_info = {AUDIT_EVENT_DETAILS_USER_NAME: target_user_name}

    try:
        if caller_user_name is None:
            raise Exception('unable to audit event, Username is empty')

        audit_service = AuditTrailService(customer)

        if target_user_group is not None:
            audit_additional_info[AUDIT_EVENT_DETAILS_USER_ROLE] = target_user_group

        audit_service.send_audit_trail_details(user_name=caller_user_name,
                                               user_group=caller_user_group,
                                               event_type=event_type,
                                               event_details=audit_additional_info,
                                               event_status=event_status)
        return True
    except Exception as ex:
        logging.error(f'audit failed: event type:{event_type}, event status:{event_status}, '
                      f'username:{caller_user_name}, event details:{audit_additional_info}  exception:{ex}')
        return False
