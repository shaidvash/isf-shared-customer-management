import logging
import os
from pydantic import ValidationError
from kobura_customer_shared.consts import *
from kobura_customer_shared.token_handler import TokenHandler
from kobura_customer_shared.dynamo_handler import DynamoHandler
from kobura.error_handling.kobura_exception import KoburaException
from kobura.error_handling.error_codes import INVALID_TOKEN, BAD_INPUT
from kobura_customer_shared.data_classes.validate_token_data import ValidateTokenData


def internal_validate_token(event):
    try:
        validate_data = ValidateTokenData(**event)
    except ValidationError as ex:
        logging.error(f'Failed change user group. Validation error: {ex}')
        raise KoburaException('Validation Error', BAD_INPUT)

    customer_name: str = validate_data.customer_name.lower()
    logging.info(f'Getting the token from sm for customer: {customer_name} .')

    token_secret = TokenHandler.get_customer_token_secret_from_sm(customer_name, validate_data.user_name)
    if not token_secret:
        raise KoburaException(f'Token secret does not exist for customer: {customer_name} .', INVALID_TOKEN)

    # validating the token
    logging.info(f'Validating the token for customer: {customer_name} .')
    response = TokenHandler.validate_token(validate_data.jwt_token, token_secret)
    if not response or not response.get('payload'):
        raise KoburaException(f'The token is invalid for customer: {customer_name}.', INVALID_TOKEN)

    if response['payload'].get(CUSTOMER_NAME) != customer_name:
        raise KoburaException(f'The token does not belong to the given customer: {customer_name} .', INVALID_TOKEN)

    # comparing token to the token from the dynamodb

    customer_from_dynamo = DynamoHandler.get_customer(
        {CUSTOMER_NAME: customer_name, CUSTOMER_USER_NAME: validate_data.user_name},
        os.environ[REGISTRATIONS_TABLE]
    )

    if not customer_from_dynamo or not customer_from_dynamo.get(TOKEN_ATTRIBUTE_NAME) \
            or customer_from_dynamo.get(TOKEN_ATTRIBUTE_NAME) != validate_data.jwt_token:
        raise KoburaException(f'The token does not exist in the db. for customer: {customer_name} .', INVALID_TOKEN)

    logging.info(f'The given token is valid for customer: {customer_name}.')
    return customer_from_dynamo
