import boto3
from kobura_customer_shared.consts import *
import os

def create_table(table_name):
    dynamo_db_resource = boto3.resource('dynamodb', region_name=os.environ[KOBURA_REGION])
    dynamo_db_resource.create_table(TableName=table_name, KeySchema=[
        {
            'AttributeName': 'customerName',
            'KeyType': 'HASH'
        }
    ], AttributeDefinitions=[
        {
            'AttributeName': 'customerName',
            'AttributeType': 'S'
        }
    ],
        ProvisionedThroughput={
            'ReadCapacityUnits': 1,
            'WriteCapacityUnits': 1
        })


def validate_ses_email(customer_email):
    ses_client = boto3.client('ses', region_name=os.environ[KOBURA_REGION])
    ses_client.verify_email_identity(EmailAddress=customer_email)
    ses_client.verify_email_identity(EmailAddress=SENDER.format(os.environ[DOMAIN_NAME]))

def create_tables():
    os.environ[CUSTOMER_TABLE] = 'customers'
    os.environ[POOLS_TABLE] = 'pools'
    os.environ[REGISTRATIONS_TABLE] = 'registrations'
    os.environ[LICENSES_TABLE] = 'licenses'
    create_table('customers')
    create_table('pools')
    create_table('registrations')
    create_table('licenses')


def create_customer_row(customer_data, table_name):
    dynamodb_resource = boto3.resource('dynamodb', region_name=os.environ[KOBURA_REGION])
    table = dynamodb_resource.Table(table_name)
    table.put_item(Item=customer_data)


def create_user_pool(pool_name):
    cognito_client = boto3.client('cognito-idp', os.environ[KOBURA_REGION])
    response = cognito_client.create_user_pool(PoolName=pool_name)
    return response['UserPool']['Id']


def create_user(pool_id, username):
    cognito_client = boto3.client('cognito-idp', os.environ[KOBURA_REGION])
    response = cognito_client.admin_create_user(
        UserPoolId=pool_id,
        Username=username
    )

def helper_create_authorizer_object(customer_name: str):
    return {
        'token': 'dfkjghbfdsjkhgdfjgfdf',
        'cognitoGroup': 'admins',
        'cognitoRole': 'aws/dsd/sd/sd:2344365',
        'cognitoUsername': 'gorge',
        'customerName': customer_name,
        'identityPoolId': 'sdf-sdfdsfds',
        'userPoolId': 'sss-2222'
    }
