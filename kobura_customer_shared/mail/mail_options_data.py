from typing import List

from kobura.validation.kob_validation import KobValidation
from pydantic import Field

from kobura_customer_shared.consts import DESTINATIONS_OPTION, SOURCE_OPTION, SUBJECT_OPTION


class MailOptionsData(KobValidation):
    destinations: List[str] = Field(..., alias=DESTINATIONS_OPTION, min_items=1)
    source: str = Field(..., alias=SOURCE_OPTION)
    subject: str = Field(..., alias=SUBJECT_OPTION)

    class Config:
        max_anystr_length = 100
