#!/usr/bin/env bash
set -e   # set -o errexit
set -u   # set -o nounset
set -o pipefail
[ "x${DEBUG:-}" = "x" ] || set -x


__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
__script="${__dir}/$(basename "${BASH_SOURCE[0]}")"
__start_dir=`pwd`

function ShowUsage
{
    echo "Usage: $0 -b <build_environment> -d <project_directory>"
}

function RunBuildSteps
{
    pushd ${PROJECT_DIR}
    RunSharedBuildSteps
    RunTests
    # TagMasterBranch
    popd
}

BUILD_ENV=
PROJECT_DIR=

while getopts b:d:h o
do  case "$o" in
  b)  BUILD_ENV="$OPTARG";;
  d)  PROJECT_DIR="$OPTARG";;
  [?] | h) ShowUsage ; exit 1;;
  esac
done

if [[ -z "${BUILD_ENV}" || -z "${PROJECT_DIR}" ]]; then
  ShowUsage
  exit 1
fi

source ${PROJECT_DIR}/isf-devops-shared/shared.sh
Export${BUILD_ENV}EnvironmentVars
RunBuildSteps
