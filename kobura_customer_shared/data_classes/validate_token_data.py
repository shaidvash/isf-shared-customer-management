from kobura.validation.kob_validation import KobValidation
from kobura.validation.regex_patterns import REGEX_COGNITO_USERNAME
from pydantic import Field

from kobura_customer_shared.consts import KOB_AUTHORIZER_TOKEN, CUSTOMER_NAME, CUSTOMER_USER_NAME, ORGANIZATION_ROOT


class ValidateTokenData(KobValidation):
    jwt_token: str = Field(..., alias=KOB_AUTHORIZER_TOKEN, min_length=1)
    customer_name: str = Field(..., alias=CUSTOMER_NAME, min_length=3, max_length=40)
    user_name: str = Field(ORGANIZATION_ROOT, alias=CUSTOMER_USER_NAME, min_length=2, max_length=30,
                           regex=REGEX_COGNITO_USERNAME)


