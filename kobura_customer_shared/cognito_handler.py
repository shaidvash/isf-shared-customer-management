import boto3

from kobura_customer_shared.users_handler import get_cognito_user_attributes


def confirmed_users_from_group_iterator(group_name, pool_id):
    cognito_client = boto3.client('cognito-idp')
    first_time = True
    while first_time or 'NextToken' in response:
        if first_time:
            first_time = False
            response = cognito_client.list_users_in_group(UserPoolId=pool_id, GroupName=group_name)
        else:
            response = cognito_client.list_users_in_group(UserPoolId=pool_id, GroupName=group_name,
                                                          NextToken=response['NextToken'])
        users_list = response.get('Users', [])
        for user in users_list:
            status = user.get('UserStatus', None)
            if user.get('Enabled', False) and (status == 'CONFIRMED' or status == 'RESET_REQUIRED' or status == 'FORCE_CHANGE_PASSWORD'):
                cognito_user = {
                    'userName': user['Username']
                }
                attributes = user.get('Attributes')
                cognito_user.update(get_cognito_user_attributes(attributes))
                yield cognito_user