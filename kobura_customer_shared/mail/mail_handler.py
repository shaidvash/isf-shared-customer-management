import calendar
import datetime
import html
import logging
import os
from typing import List

import boto3
from pydantic import ValidationError

from kobura_customer_shared.consts import CHARSET, DESTINATIONS_OPTION, SOURCE_OPTION, SENDER, DOMAIN_NAME, \
    SUBJECT_OPTION, WELCOME_SUBJECT, \
    SIGNUP_URL, FORGOT_PASSWORD_SUBJECT, RESET_PASSWORD_URL, FOLLOW_SUBJECT, SIGNIN_URL, \
    EXPIRY_DAYS_TO_GO
from kobura_customer_shared.mail.cem_images import REGULAR_MAIL_IMG, BIG_IMG_LOW_RESOLUTION, BIG_MAIL_IMG
from kobura_customer_shared.mail.mail_consts import WELCOM_MAIL_TEXT, REGULAR_IMG_TAG, WELCOM_MAIL_TEXT_HTML, \
    REGULAR_MAIL_IMG_NAME, GENERIC_MAIL_HTML, BODY_TEXT_FOLLOW_UP, BODY_FOLLOW_UP_TEXT_HTML, INVITATION_MAIL_TEXT, \
    INVITATION_MAIL_TEXT_HTML, FOLLOW_IMG_TAG, FOLLOW_IMG_NAME, \
    FORGOT_PASSWORD_MAIL_TEXT, FORGOT_PASSWORD_MAIL_TEXT_HTML, GENERIC_BUTTON, \
    WELCOME_BUTTON_TEXT, FORGOT_PASSWORD_BUTTON_TEXT, FOLLOW_UP_BUTTON_TEXT, PRE_EXPIRY_EMAIL, \
    PRE_EXPIRY_EMAIL_HTML, NO_BUTTON, INTERNAL_EMAIL
from kobura_customer_shared.mail.mail_options_data import MailOptionsData


class MailHandler:

    def __init__(self, full_system_name, mail_client=None):
        if mail_client:
            self.client = mail_client
        else:
            self.client = boto3.client('ses')

        self.full_system_name = full_system_name

    def __send_mail(self, mail_options, mail_data_text, mail_data_html=''):
        try:
            options = MailOptionsData(**mail_options)
        except ValidationError as ex:
            logging.error(f'Failed to validate mail options data. Error: {ex}.')
            raise Exception('Invalid mail options data.')

        self.client.send_email(
            Destination={
                'ToAddresses': options.destinations,
            },
            Message={
                'Body': {
                    'Html': {
                        'Charset': CHARSET,
                        'Data': mail_data_html,
                    },
                    'Text': {
                        'Charset': CHARSET,
                        'Data': mail_data_text,
                    },
                },
                'Subject': {
                    'Charset': CHARSET,
                    'Data': options.subject,
                },
            },
            Source=options.source
        )

    def send_internal_mail(self, subject, mail_data_text, mail_data_html='', emails: List[str] = None):
        if not emails:
            emails = [self.__get_email()]
        internal_mail_options = {
            DESTINATIONS_OPTION: emails,
            SOURCE_OPTION: SENDER.format(os.environ[DOMAIN_NAME]),
            SUBJECT_OPTION: subject
        }
        self.__send_mail(internal_mail_options, mail_data_text, mail_data_html)

    def send_welcome_email(self, user_email, organization, token):
        welcome_mail_subject = self.__get_welcome_mail_subject()
        mail_options = {
            DESTINATIONS_OPTION: [user_email],
            SOURCE_OPTION: SENDER.format(os.environ[DOMAIN_NAME]),
            SUBJECT_OPTION: welcome_mail_subject
        }

        signup_url = SIGNUP_URL.format(os.environ[DOMAIN_NAME], token, self.__sanitize(organization))
        mail_data_text: str = self.__get_welcome_mail_text().format(url=signup_url, fullSystemName=self.full_system_name)

        mail_data_html = self.__generate_html(
            outlook_png=self.__get_regular_img(),
            img_tag=(
                REGULAR_IMG_TAG.format(domain=os.environ[DOMAIN_NAME], imgName=self.__get_regular_mail_img_name())),
            body_text=self.__get_welcome_mail_text_html(),
            button_area=GENERIC_BUTTON.format(linkUrl=signup_url, buttonText=self.__get_welcome_button_text())
        )

        self.__send_mail(mail_options, mail_data_text, mail_data_html)

        self.send_internal_mail(welcome_mail_subject, mail_data_text, mail_data_html)

    def send_forgot_password_email(self, user_email, customer_name, user_name, user_full_name, token):
        subject = self.__get_forgot_password_mail_subject()
        mail_options = {
            DESTINATIONS_OPTION: [user_email],
            SOURCE_OPTION: SENDER.format(os.environ[DOMAIN_NAME]),
            SUBJECT_OPTION: subject
        }

        # should change the url to go to the reset password page
        signup_url = RESET_PASSWORD_URL.format(os.environ[DOMAIN_NAME], token, self.__sanitize(customer_name),
                                                      self.__sanitize(user_name))
        mail_data_text: str = self.__get_forgot_password_mail_text().format(username=self.__sanitize(user_full_name),
                                                                            fullSystemName=self.full_system_name,
                                                                            url=signup_url)

        body_text = self.__get_forgot_password_mail_text_html().format(userName=self.__sanitize(user_full_name),
                                                                       fullSystemName=self.full_system_name)
        mail_data_html = self.__generate_html(
            outlook_png=self.__get_regular_img(),
            img_tag=(
                REGULAR_IMG_TAG.format(domain=os.environ[DOMAIN_NAME], imgName=self.__get_regular_mail_img_name())),
            body_text=body_text,
            button_area=GENERIC_BUTTON.format(linkUrl=signup_url, buttonText=FORGOT_PASSWORD_BUTTON_TEXT)
        )

        self.__send_mail(mail_options, mail_data_text, mail_data_html)

    def send_pre_expiry_mail(self, customer_name, user_email, user_full_name, license_type, days_left,
                             final_expiry_date):
        mail_options = {
            DESTINATIONS_OPTION: [user_email],
            SOURCE_OPTION: SENDER.format(os.environ[DOMAIN_NAME]),
            SUBJECT_OPTION: self.__get_pre_expiry_mail_subject().format(days_left, self.__sanitize(customer_name), self.full_system_name)
        }

        str_expiry_date = f'{calendar.day_name[final_expiry_date.weekday()]}, {calendar.month_name[final_expiry_date.month]} {final_expiry_date.day}'
        mail_data_text: str = self.__get_pre_expiry_mail_text().format(userFullName=self.__sanitize(user_full_name),
                                                                       licenseType=license_type,
                                                                       customerName=self.__sanitize(customer_name),
                                                                       fullSystemName=self.full_system_name,
                                                                       daysLeft=days_left,
                                                                       expiryDate=str_expiry_date)

        mail_data_html = self.__generate_html(
            outlook_png=self.__get_regular_img(),
            img_tag=(
                REGULAR_IMG_TAG.format(domain=os.environ[DOMAIN_NAME], imgName=self.__get_regular_mail_img_name())),
            body_text=self.__get_pre_expiry_mail_html().format(license_type=license_type,
                                                               customer_name=self.__sanitize(customer_name),
                                                               user_name=self.__sanitize(user_full_name),
                                                               days_left=days_left,
                                                               expiry_date=str_expiry_date,
                                                               fullSystemName=self.full_system_name),
            button_area=NO_BUTTON
        )

        self.__send_mail(mail_options, mail_data_text, mail_data_html)

    def __generate_html(self, outlook_png, img_tag, body_text, button_area):
        mail_data_html: str = self.__get_generic_mail_HTML().format(outlookPng=outlook_png,
                                                                    imgTag=img_tag,
                                                                    bodyText=body_text,
                                                                    buttonArea=button_area,
                                                                    year=datetime.datetime.now().year,
                                                                    fullSystemName=self.full_system_name)
        return mail_data_html

    def send_follow_up_email(self, user_email, organization, user_name, user_full_name, password, sender_name=None):
        mail_options = {
            DESTINATIONS_OPTION: [user_email],
            SOURCE_OPTION: SENDER.format(os.environ[DOMAIN_NAME]),
            SUBJECT_OPTION: self.__get_follow_up_mail_subject()
        }

        if sender_name is None:
            mail_data_text = self.__get_follow_up_mail_text().format(fullSystemName=self.full_system_name,
                                                                     customerName=self.__sanitize(organization),
                                                                     userName=self.__sanitize(user_name),
                                                                     password=password,
                                                                     url=SIGNIN_URL.format(os.environ[DOMAIN_NAME]))
            text_html = self.__get_follow_up_mail_text_html().format(fullSystemName=self.full_system_name,
                                                                     organization=organization,
                                                                     userName=self.__sanitize(user_name),
                                                                     password=password)
        else:
            mail_data_text = self.__get_invitation_mail_text().format(userFullName=self.__sanitize(user_full_name),
                                                                      senderName=sender_name,
                                                                      customerName=self.__sanitize(organization),
                                                                      fullSystemName=self.full_system_name,
                                                                      userName=self.__sanitize(user_name),
                                                                      password=password,
                                                                      url=SIGNIN_URL.format(os.environ[DOMAIN_NAME]))
            text_html = self.__get_invitation_mail_text_html().format(userFullName=self.__sanitize(user_full_name),
                                                                      senderName=sender_name,
                                                                      fullSystemName=self.full_system_name,
                                                                      organization=self.__sanitize(organization),
                                                                      userName=self.__sanitize(user_name),
                                                                      password=password)

        mail_data_html = self.__generate_html(
            outlook_png=self.__get_big_img_low_resolution(),
            img_tag=FOLLOW_IMG_TAG.format(domain=os.environ[DOMAIN_NAME], imgName=self.__get_follow_up_img_name()),
            body_text=text_html,
            button_area=GENERIC_BUTTON.format(linkUrl=SIGNIN_URL.format(os.environ[DOMAIN_NAME]),
                                              buttonText=self.__get_follow_up_button_text())
        )

        self.__send_mail(mail_options, mail_data_text, mail_data_html)

        self.send_internal_mail(self.__get_follow_up_mail_subject(), mail_data_text, mail_data_html)

    def __sanitize(self, string: str) -> str:
        return html.escape(string)

    def __get_email(self) -> str:
        return INTERNAL_EMAIL

    def __get_regular_mail_img_name(self) -> str:
        return REGULAR_MAIL_IMG_NAME

    def __get_generic_mail_HTML(self) -> str:
        return GENERIC_MAIL_HTML

    def __get_follow_up_img_name(self) -> str:
        return FOLLOW_IMG_NAME

    def __get_welcome_mail_subject(self) -> str:
        return WELCOME_SUBJECT.format(fullSystemName=self.full_system_name)

    def __get_welcome_mail_text(self) -> str:
        return WELCOM_MAIL_TEXT

    def __get_welcome_mail_text_html(self) -> str:
        return WELCOM_MAIL_TEXT_HTML.format(fullSystemName=self.full_system_name)

    def __get_welcome_button_text(self) -> str:
        return WELCOME_BUTTON_TEXT.format(fullSystemName=self.full_system_name)

    def __get_forgot_password_mail_text(self) -> str:
        return FORGOT_PASSWORD_MAIL_TEXT

    def __get_forgot_password_mail_subject(self) -> str:
        return FORGOT_PASSWORD_SUBJECT.format(fullSystemName=self.full_system_name)

    def __get_forgot_password_mail_text_html(self) -> str:
        return FORGOT_PASSWORD_MAIL_TEXT_HTML

    def __get_pre_expiry_mail_html(self) -> str:
        return PRE_EXPIRY_EMAIL_HTML

    def __get_pre_expiry_mail_text(self) -> str:
        return PRE_EXPIRY_EMAIL

    def __get_pre_expiry_mail_subject(self) -> str:
        return EXPIRY_DAYS_TO_GO

    def __get_follow_up_mail_text(self) -> str:
        return BODY_TEXT_FOLLOW_UP

    def __get_follow_up_mail_subject(self) -> str:
        return FOLLOW_SUBJECT.format(fullSystemName=self.full_system_name)

    def __get_follow_up_mail_text_html(self) -> str:
        return BODY_FOLLOW_UP_TEXT_HTML

    def __get_follow_up_button_text(self) -> str:
        return FOLLOW_UP_BUTTON_TEXT.format(fullSystemName=self.full_system_name)

    def __get_invitation_mail_text(self) -> str:
        return INVITATION_MAIL_TEXT

    def __get_invitation_mail_text_html(self) -> str:
        return INVITATION_MAIL_TEXT_HTML

    def __get_big_img_low_resolution(self) -> str:
        return BIG_IMG_LOW_RESOLUTION

    def __get_regular_img(self) -> str:
        return REGULAR_MAIL_IMG

    def __get_big_img(self) -> str:
        return BIG_MAIL_IMG
