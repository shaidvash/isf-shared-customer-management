import logging
import os
from typing import Optional

from kobura_customer_shared.consts import CUSTOMER_TABLE, CUSTOMER_NAME, CUSTOMER_USER_NAME, REGISTRATIONS_TABLE
from kobura_customer_shared.dynamo_handler import DynamoHandler


def check_if_customer_item_exists_in_table(customer_name: str, table_name: Optional[str]=None):
    if table_name is None:
        table_name = os.environ.get(CUSTOMER_TABLE, None)
        if not table_name:
            raise Exception(f'{CUSTOMER_TABLE} env var is not defined')

    try:
        customer = DynamoHandler.get_customer({CUSTOMER_NAME: customer_name}, table_name, consistent_read=True)
        if customer is not None:
            return True
        return False
    except Exception as ex:
        logging.error(f'Error while getting customer {customer_name}, error: {str(ex)}')
        raise


def check_if_customer_item_exists_in_registrations(customer_name: str, user_name: str,
                                                   table_name: Optional[str] = None):
    if table_name is None:
        table_name = os.environ.get(REGISTRATIONS_TABLE, None)
        if not table_name:
            raise Exception(f'{REGISTRATIONS_TABLE} env var is not defined')

    try:
        customer = DynamoHandler.get_customer({CUSTOMER_NAME: customer_name, CUSTOMER_USER_NAME: user_name},
                                              table_name, consistent_read=True)
        if customer is not None:
            return True
        return False
    except Exception as ex:
        logging.error(f'Error while getting customer {customer_name}, error: {str(ex)}')
        raise
